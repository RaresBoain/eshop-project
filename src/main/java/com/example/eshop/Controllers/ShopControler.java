package com.example.eshop.Controllers;

import com.example.eshop.entities.Category;
import com.example.eshop.entities.Product;
import com.example.eshop.repository.CategoryRepository;
import com.example.eshop.repository.ProductRepository;
import com.example.eshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class ShopControler {

    private final UserRepository userRepository;

    private final CategoryRepository categoryRepository;

    @Autowired
    public ShopControler(UserRepository userRepository, CategoryRepository categoryRepository) {
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/")
    public String showAllProducts(Model model){
        List<Category> categories = (List<Category>) categoryRepository.findAll();
        model.addAttribute("categories", categories);
        return "index";

    }

    @GetMapping("/categories/{id}")
    public String showProductsSet(@PathVariable("id") int id, Model model){
        Category category = categoryRepository.findById(id).get();
        model.addAttribute("category", category);
        return "product_list";

    }

}
