package com.example.eshop.service;

import com.example.eshop.entities.User;

import java.util.List;

public interface IUserService {
    List<User> findAll();
}
