package com.example.eshop.service;

import com.example.eshop.entities.Product;
import com.example.eshop.entities.User;

import java.util.List;

public interface IProductService {
    List<Product> findAll();
}
