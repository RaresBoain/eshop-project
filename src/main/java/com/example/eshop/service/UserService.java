package com.example.eshop.service;

import com.example.eshop.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.eshop.repository.UserRepository;

import java.util.List;

public class UserService implements IUserService {
    @Autowired
    private UserRepository repository;

    @Override
    public List<User> findAll(){
        return (List<User>) repository.findAll();

    }
}
