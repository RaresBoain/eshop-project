package com.example.eshop.service;

import com.example.eshop.entities.Product;
import com.example.eshop.entities.User;
import com.example.eshop.repository.ProductRepository;
import com.example.eshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProductService implements IProductService{
    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> findAll() {
        return (List<Product>) productRepository.findAll();
    }
}
