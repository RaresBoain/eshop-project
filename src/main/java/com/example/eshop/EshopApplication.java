package com.example.eshop;

import com.example.eshop.entities.Category;
import com.example.eshop.entities.Product;
import com.example.eshop.entities.User;
import com.example.eshop.repository.CategoryRepository;
import com.example.eshop.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.example.eshop.repository.UserRepository;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class EshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(EshopApplication.class, args);

	}
//
//	@Bean
//	public CommandLineRunner run(UserRepository userRepository) {
//		return (String[] args) -> {
//			User user1 = new User("John", "john@domain.com");
//			User user2 = new User("Julie", "julie@domain.com");
//			User user3 = new User("Rares", "ppppwwwwdddd");
////			userRepository.save(user1);
////			userRepository.save(user2);
////			userRepository.save(user3);
//			userRepository.findAll().forEach(user -> System.out.println(user));
//		};
//	}

//	@Bean
//	public CommandLineRunner run(ProductRepository productRepository, CategoryRepository categoryRepository) {
//		return (String[] args) -> {
//			Category c1 = new Category("Metallurgical products");
//			categoryRepository.save(c1);
//			Product product1 = new Product("Tabla neagra 10x1500x6000", "S235JR", 2.5, 20);
//			Product product2 = new Product("Tabla decapata 2x1000x2000", "DC01", 4.5, 50);
//			Product product3 = new Product("Teava rectangulara 20x20x2x6000", "S355J2",23, 100);
//			Category c1 = categoryRepository.findAll().iterator().next();
//			product1.setCategory(c1);
//			product2.setCategory(c1);
//			product3.setCategory(c1);
//			productRepository.save(product1);
//			productRepository.save(product2);
//			productRepository.save(product3);
//			productRepository.findAll().forEach(product -> System.out.println(product));
//		};
//	}
}
